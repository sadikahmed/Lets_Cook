
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Contact Us</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
<style>
.jumbotron {
		background: #EEEEEE;
		color: #FFF;
		border-radius: 0px;
		color: black;
}
.jumbotron-sm {
		padding-top: 24px;
		padding-bottom: 24px;
		 }
.jumbotron small {
		color: black;
}
.h1 small {
		font-size: 35px;
}
</style>
</head>
<body>

<!-- Navigation Bar -->
<nav class="navbar navbar-inverse navbar-fixed-top " id ="my-navbar">
    <div class="container">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand"> Let's Cook </a>
        </div>  <!-- navbar Header -->
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
              <li ><a href="index.php">Home</a></li>
              <li ><a href="topRecipe.php">Popular Recipes</a></li>
              <li><a href="recentlyAdded.php">Recently Added</a></li>
              <li><a href="help.php">Help</a></li>
              <li class="active"><a href="contact.php">Contact Us</a></li>
            </ul>
        </div>  <!-- End Collapse Navbar -->   
    </div> <!-- End Container --> 
</nav> <!-- End Navbar -->


<div class="jumbotron jumbotron-sm">
    <div class="container" style="margin-top: 30px;">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h1 class="h1">
                    <small>Feel free to contact us</small></h1>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="well well-sm">
                <form>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Enter name" required="required" />
                        </div>
                        <div class="form-group">
                            <label for="email">
                                Email Address</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                                </span>
                                <input type="email" class="form-control" id="email" placeholder="Enter email" required="required" /></div>
                        </div>
                        <div class="form-group">
                            <label for="subject">
                                Subject</label>
                            <select id="subject" name="subject" class="form-control" required="required">
                                <option value="na" selected="">Choose One:</option>
                                <option value="service">General Customer Service</option>
                                <option value="suggestions">Suggestions</option>
                                <option value="product">Product Support</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                Message</label>
                            <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required"
                                placeholder="Message"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary pull-right" id="btnContactUs" style="background-color: #EC971F;">
                            Send Message</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <div class="col-md-4">
            <form>
            <legend><span class="glyphicon glyphicon-globe"></span> Our Address</legend>
            <address>
                <strong>University of Liberal Arts Bangladesh</strong><br>
                House 56, Rd 4/A @ Satmasjid Road Dhanmondi,<br>
                Dhaka-1209, Bangladesh<br>
                <abbr title="Phone">
                    PH:</abbr>
                +8801929622802
            </address>
            <address>
                <strong> Email us : </strong><br>
                <a href="mailto:mranjon@gmail.com">sadik101294@gmail.com</a> <br>
 <!--               <a href="mailto:mranjon@gmail.com">gautam773@gmail.com</a> <br>
                <a href="mailto:mranjon@gmail.com">nuha.khan4@gmail.com</a> <br> -->
            </address>
            </form>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>