      <!-- Navigation Bar -->    
     <nav class="navbar navbar-inverse navbar-fixed-top " id ="my-navbar">
      <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand"><strong> <i> Let's Cook  </i> </strong> </a>
          </div>  <!-- navbar Header -->
          <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
              <li><a href="index.php">Home</a></li>
              <li><a href="topRecipe.php">Popular Recipes</a></li>
              <li><a href="recentlyAdded">Recently Added</a></li>
              <li><a href="help.php">Help</a></li>
              <li><a href="contact.php">Contact Us</a></li>
            </ul>
          </div>  <!-- End Collapse Navbar -->
            
      </div> <!-- End Container --> 
    </nav> <!-- End Navbar -->
