<?php 
define("DB_SERVER", "localhost");
define("DB_USER", "root");
define("DB_PASS", "");
define("DB_NAME", "imagetestdb");

$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);

// Connection varification
if(mysqli_connect_errno()){
    die("Database connection failed: ".mysqli_connect_error()."(".mysqli_connect_errno().")");
    }


?>




<?php include_once('header.php');  ?>

  <body>
      <!-- Navigation Bar -->
               
     <nav class="navbar navbar-inverse navbar-fixed-top " id ="my-navbar">
      <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand"><strong> <i> Let's Cook  </i> </strong></a>
          </div>  <!-- navbar Header -->
          <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="index.php">Home</a></li>
              <li><a href="topRecipe.php">Popular Recipes</a></li>
              <li><a href="recentlyAdded.php">Recently Added</a></li>
              <li><a href="help.php">Help</a></li>
              <li><a href="contact.php">Contact Us</a></li>
            </ul>
          </div>  <!-- End Collapse Navbar -->
            
      </div> <!-- End Container --> 
    </nav> <!-- End Navbar -->







  <div> <!-- Start div for carasoul -->
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
      <li data-target="#carousel-example-generic" data-slide-to="1"></li>
      <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="image/img1.jpg" alt="slider1">
        <div class="carousel-caption">
          ...
        </div>
      </div>
      <div class="item">
        <img src="image/img3.jpg" alt="Slider2">
        <div class="carousel-caption">
          ...
        </div>
      </div>
      <div class="item">
        <img src="image/img4.jpg" alt="Slider3">
        <div class="carousel-caption">
          ...
        </div>
      </div>
    </div>

    <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div> <!-- Start div for carasoul -->







<!-- jumbotron -->

  <div class="jumbotron">  
    <div class="container text-center" style="margin-top: 20px;">
      <h1 style="font-size:30px;"> <strong>Ingredients List</strong></h1>
      



      <div style="margin-top: 30px;">
        <p style="font-size:18px;"><i> 
          <b> Just Select your available ingridents, We will find you the best possible Recipes. </b></i>
        </p>  
      </div>
      
      <form role="form" action="second.php" method="POST"  enctype="multipart/form-data"> 

      <?php
          // query for show the number of recipe found on given ingredients
          $qry="select distinct ingredient_name from ingredient_list order by ingredient_name";
          $result=mysqli_query($connection, $qry);
          $count = mysqli_num_rows($result);

          //echo "Start($i): <input type='text' name='start"."$i'>";

          $j = 1;
          while($count>0)
          {
            $i = 1;

            echo '<div class="row" >'; 
            while($row = mysqli_fetch_array($result))
            {   

                echo '<div class="col-lg-3">';
                echo '<div style="padding-left:50px;" class="checkbox pull-left" >'; //style="padding-left:40px;" 
                $na = $row["ingredient_name"];
                
                if(count(explode(' ', $na)) > 0) { 
                    $na = str_replace(" ","_",$na);
                 }
                 // class="pull-left"
                 echo '<label ><input type="checkbox" value="'.$row["ingredient_name"].'" name="'.$na.'" >'.$row["ingredient_name"].'</label>';
                  
                echo '</div>';
                echo '</div>';
                $count--;
                if($i%4==0)
                {
                    break;
                }
                $i +=1;
            }
            echo '</div>';
          }

      ?>



    <div class="row" style="margin-top: 20px;">
        <div>    
            <label style="font-size: 15px; margin-left: 0px;">Serves For</label>
            <select name="ServeFor" style="border-radius: 5px;" class="dropdown">
              <option value="4">4 persons</option>
              <option value="6">6 persons</option>
              <option value="2" selected>2 persons</option>
            </select>
        </div>
    </div>


      <div class="row">
        <div style="margin-top: 30px; margin-left: 0px;" >
          <button  type="submit" class="btn btn-lg btn-warning" class="center" name="submit">Search</button>
        </div>
      </div>

      </form>


      <?php
        //$qry="select distinct ingredient_name from ingredient_list order by ingredient_name";
        //$result=mysqli_query($connection, $qry);
        //$count = mysqli_num_rows($result);

        //if(isset($_POST["submit"]))
        //{
        //  $i=1;
        //    while( $row = mysqli_fetch_array($result))
        //    //for($i=1; $i<$count; $i++)
        //    {
        //      $na = $row["ingredient_name"];
        //        
        //      if(count(explode(' ', $na)) > 0) 
        //      { 
        //        $na = str_replace(" ","_",$na);
        //      }

        //      if(isset($_POST[$na]))
        //      {
        //          echo $_POST[$na]."<br>";
        //      }

        //    }
        //}
      ?>

    </div> <!--End of container text-center -->

  </div><!-- End of jumbotron -->


<?php include("footer.php");  ?>