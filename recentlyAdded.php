<?php 
define("DB_SERVER", "localhost");
define("DB_USER", "root");
define("DB_PASS", "");
define("DB_NAME", "imagetestdb");

$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);

// Connection varification
if(mysqli_connect_errno()){
    die("Database connection failed: ".mysqli_connect_error()."(".mysqli_connect_errno().")");
    }


?>




<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Recently Added</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
  </head>



  <body>
      <!-- Navigation Bar -->
               
     <nav class="navbar navbar-inverse navbar-fixed-top " id ="my-navbar">
      <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand"><strong> <i> Let's Cook  </i> </strong></a>
          </div>  <!-- navbar Header -->
          <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
              <li ><a href="index.php">Home</a></li>
              <li><a href="topRecipe.php">Popular Recipes</a></li>
              <li class="active"><a href="recentlyAdded.php">Recently Added</a></li>
              <li><a href="help.php">Help</a></li>
              <li><a href="contact.php">Contact Us</a></li>
            </ul>
          </div>  <!-- End Collapse Navbar -->
            
      </div> <!-- End Container --> 
    </nav> <!-- End Navbar -->





<?php
      $page = 0;
      if(isset($_GET['page']))
      {
          $page = (int)$_GET['page'];
          $page = ($page*6) -6;
      }
      
        $query_for_view_recipe_list = "Select * from recipe_list where add_date > DATE_SUB(NOW(), INTERVAL 1 WEEK) order by add_date desc limit $page, 6";
        //SELECT * FROM jokes WHERE date > DATE_SUB(NOW(), INTERVAL 1 WEEK) ORDER BY score DESC;
        $result_for_view_recipe_list = mysqli_query($connection, $query_for_view_recipe_list);
        $number_of_matched_recipe = mysqli_num_rows($result_for_view_recipe_list);

?>




 <?php     

    $query_for_view_recipe_list_number_in_header = "Select * from recipe_list where add_date > DATE_SUB(NOW(), INTERVAL 1 WEEK) order by add_date desc";
    $result_for_view_recipe_list_number_in_header = mysqli_query($connection, $query_for_view_recipe_list_number_in_header);
    $number_of_matched_recipe_number_view_in_header = mysqli_num_rows($result_for_view_recipe_list_number_in_header); 
 
?>

  <div class="jumbotron"> <!-- Start Div jumbotron --> 

    <div class="text-center" > <!-- Start Div for Recipe's number -->
    <h1 style="font-size: 40px"><?php echo $number_of_matched_recipe_number_view_in_header; ?> new recipes are added in the last week. </h1>
    </div> <!-- End Div for Recipe's number -->

    <!-- show result from Database -->
    
<?php

      //$count_for_paging = mysqli_num_rows($result_for_view_recipe_list);
      //$count = $count_for_paging;
      //echo $count."<br>";
      $count = $number_of_matched_recipe;
      while($count>0)
      {
          $i = 1; 
          echo '<div class = "container" style="margin-top: 50px; ">';
          echo '<div class="row" >'; 
          while($row = mysqli_fetch_array($result_for_view_recipe_list))
          {   
              echo '<div class="col-lg-4">';

              echo '<a href="third.php?id='.$row["id"].'"><h4>'.$row["recipe_name"].'</h4></a>';
              echo '<img src="data:image/jpeg;base64,'.base64_encode($row["picture"]).'" width="300px" height="200px"/>';
              //echo '<img src="data:image/jpeg;base64,'.base64_encode( $result['image'] ).'"/>'; 
              echo '</div>';
              $count--;
              if($i%3==0)
              {
                  break;
              }
              $i +=1;    
          }
          echo '</div>';
          echo '</div>'; 
      }
?>


<!-- Pagination -->

<div>

<?php 
      //$qry="select * from images";
      //$result=mysqli_query($connection, $qry);
      //$count = mysqli_num_rows($result);

      $page_num = ceil($number_of_matched_recipe_number_view_in_header/6);
      //echo $page_num."<br>";
      //echo $page_num."<br><br><br>"; 


?>
  <div class="row text-center">
    <div class="col-lg-12">   
       <ul class="pagination">
          <li>
            <a href="#">&laquo;</a>
          </li>

    <?php 
          for($i=1; $i<=$page_num; $i++){ ?>

          <li 
          <?php if(isset($_GET["page"])){
            if($_GET["page"]==$i)
              {echo 'class="active"';}
            } ?> 
            ><a href="recentlyAdded.php?page=<?php echo $i; ?>" > <?php echo $i; ?></a></li>

    <?php  } ?>

          <li>
            <a href="#">&raquo;</a>
          </li>

        </ul>
    </div>
  </div>

</div>

<!-- End of Pagination -->
