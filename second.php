<?php
  session_start();
?>

<?php 
define("DB_SERVER", "localhost");
define("DB_USER", "root");
define("DB_PASS", "");
define("DB_NAME", "imagetestdb");

$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);

// Connection varification
if(mysqli_connect_errno()){
    die("Database connection failed: ".mysqli_connect_error()."(".mysqli_connect_errno().")");
    }

    
?>





<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Search Result</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
  </head>




  <body>
      <!-- Navigation Bar -->
          <?php  include_once('navigationBar.php');  ?>      
      <!-- End Navbar -->


<?php
        
        if(!isset($_GET["page"]))
        {
            $query_for_delete = "delete from final_list";
            $is_deleted=mysqli_query($connection, $query_for_delete);        
                
        }

        //$qry2="select distinct ingredient_name from ingredient_list order by ingredient_name";
        $query_for_all_item = "select distinct ingredient_name from ingredient_list order by ingredient_name";
        $result_all_item=mysqli_query($connection, $query_for_all_item);
        $count_for_building_query = 0;

        $query_after_selecting_item ="";

        if(isset($_POST["submit"]))
        {



            $serve_For = $_POST["ServeFor"];
            //echo $serve_For;
            $_SESSION["serveFor"] = $serve_For; 
            while($row2 = mysqli_fetch_array($result_all_item))
            {
              $na = $row2["ingredient_name"];
                
              if(count(explode(' ', $na)) > 0) 
              { 
                $na = str_replace(" ","_",$na);
              }

              if(isset($_POST[$na]))
              {
                   if($count_for_building_query > 0)
                  {
                      $query_after_selecting_item .= " Union all ";
                  }
                  $query_after_selecting_item .= 'Select distinct r.recipe_name,  r.id, r.number_of_ingredient, r.picture from recipe_list r join ingredient_list i on r.id = i.recipe_id where ingredient_name ="';
                  $query_after_selecting_item .= $_POST[$na].'"';  

                  //echo $_POST[$na]."<br>";
                  $count_for_building_query += 1;
              }

            }
            //echo $query_after_selecting_item."<br>";
            //$qry="select * from recipe_list  limit $page, 6";
            if($count_for_building_query==0)
            {

                //echo "Hellooooooooooooooooooooooooooooooooooooo"  ;
                 //echo $query_after_selecting_item."<br>";
            }
            else
            {

               $result_after_selecting_item=mysqli_query($connection, $query_after_selecting_item);
 
                $count = mysqli_num_rows($result_after_selecting_item);
                //echo $count."<br>";
                //$count1 = $count;
                
                $query_for_delete = "delete from final_list";
                $is_deleted=mysqli_query($connection, $query_for_delete);        
                
                while ($row = mysqli_fetch_array($result_after_selecting_item)) {
                //echo "Helo"."<br>";
                $query_final_list = "INSERT INTO final_list (recipe_id, recipe_name, ingredient_number)
                VALUES (".$row['id'].", '".$row['recipe_name']."' , ".$row['number_of_ingredient'].")";
                //echo $query_final_list."<br>";
                $is_row_affected = mysqli_query($connection, $query_final_list);
                //if($is_row_affected){echo "Hello<br>";} 
                
                }
            }    
        }

        //echo " helooooooooooooooooooooooooooooooooo"; 
        
?>

<?php
      $page = 0;
      if(isset($_GET['page']))
      {
          $page = (int)$_GET['page'];
          $page = ($page*6) -6;
      }
      
        $query_for_view_recipe_list = "Select r.id, r.recipe_name, r.picture, count(i.recipe_id) as number_of_match_recipe, (r.number_of_ingredient - count(i.recipe_id)) as sorting from recipe_list r join final_list i on r.id = i.recipe_id group by i.recipe_id order by (r.number_of_ingredient - count(i.recipe_id)) limit $page, 6";

        $result_for_view_recipe_list = mysqli_query($connection, $query_for_view_recipe_list);
        $number_of_matched_recipe = mysqli_num_rows($result_for_view_recipe_list);

        //echo $number_of_matched_recipe."<br>";

?>




 <?php     

    $query_for_view_recipe_list_number_in_header = "Select r.id, r.recipe_name, r.picture, count(i.recipe_id) as number_of_match_recipe, (r.number_of_ingredient - count(i.recipe_id)) as sorting from recipe_list r join final_list i on r.id = i.recipe_id group by i.recipe_id order by (r.number_of_ingredient - count(i.recipe_id))";
    $result_for_view_recipe_list_number_in_header = mysqli_query($connection, $query_for_view_recipe_list_number_in_header);
    $number_of_matched_recipe_number_view_in_header = mysqli_num_rows($result_for_view_recipe_list_number_in_header); 
 
?>

  <div class="jumbotron"> <!-- Start Div jumbotron --> 

    <div class="text-center" > <!-- Start Div for Recipe's number -->
    <h1 style="font-size: 40px"> You can make <?php echo $number_of_matched_recipe_number_view_in_header; ?> recipes </h1>
    </div> <!-- End Div for Recipe's number -->

    <!-- show result from Database -->
    
<?php

      //$count_for_paging = mysqli_num_rows($result_for_view_recipe_list);
      //$count = $count_for_paging;
      //echo $count."<br>";
      $count = $number_of_matched_recipe;
      while($count>0)
      {
          $i = 1; 
          echo '<div class = "container" style="margin-top: 50px; ">';
          echo '<div class="row" >'; 
          while($row = mysqli_fetch_array($result_for_view_recipe_list))
          {   
              echo '<div class="col-lg-4">';

              echo '<a target="_blank"  href="third.php?id='.$row["id"].'"><h4>'.$row["recipe_name"].'</h4></a>';
              echo '<img src="data:image/jpeg;base64,'.base64_encode($row["picture"]).'" width="300px" height="200px"/>';
              //echo '<img src="data:image/jpeg;base64,'.base64_encode( $result['image'] ).'"/>'; 
              echo '</div>';
              $count--;
              if($i%3==0)
              {
                  break;
              }
              $i +=1;    
          }
          echo '</div>';
          echo '</div>'; 
      }
?>










<!-- Pagination -->

<div>

<?php 
      //$qry="select * from images";
      //$result=mysqli_query($connection, $qry);
      //$count = mysqli_num_rows($result);

      $page_num = ceil($number_of_matched_recipe_number_view_in_header/6);
      //echo $page_num."<br>";
      //echo $page_num."<br><br><br>"; 


?>
  <div class="row text-center">
    <div class="col-lg-12">   
       <ul class="pagination">
          <li>
            <a href="#">&laquo;</a>
          </li>

    <?php 
          for($i=1; $i<=$page_num; $i++){ ?>

          <li 
          <?php if(isset($_GET["page"])){
            if($_GET["page"]==$i)
              {echo 'class="active"';}
            } ?> 
            ><a href="second.php?page=<?php echo $i; ?>" > <?php echo $i; ?></a></li>

    <?php  } ?>

          <li>
            <a href="#">&raquo;</a>
          </li>

        </ul>
    </div>
  </div>

</div>

<!-- End of Pagination -->



  <!-- show result from Database -->

  </div>  <!--End Div jumbotron --> 



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>





