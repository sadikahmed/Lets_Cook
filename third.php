<?php
session_start();


?>
<?php 
define("DB_SERVER", "localhost");
define("DB_USER", "root");
define("DB_PASS", "");
define("DB_NAME", "imagetestdb");

$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);

// Connection varification
if(mysqli_connect_errno()){
    die("Database connection failed: ".mysqli_connect_error()."(".mysqli_connect_errno().")");
    }

    
?>


<?php
    $id = 0;
    if(isset($_GET["id"]))
    {
      $id = $_GET["id"];

      echo $id."<br>";
    }
    $serve_for = $_SESSION["serveFor"];
    //$_SESSION["serveFor"] = null;
    $query = "select * ";
    $query .= "from recipe_list where id = ".$id;
    $result = mysqli_query($connection, $query);
    
    if(!$result)
    {
      die("database query failed.");
    }

    $query_for_update_popularity = "Update recipe_list set top_recipe = (top_recipe+1) where id = "; 
    $query_for_update_popularity .= $id;

    $is_updated = mysqli_query($connection, $query_for_update_popularity);
       

    // DELETE PREVIOUS STORIES///
    //$query_for_delete = "delete from final_list";
    //$is_deleted=mysqli_query($connection, $query_for_delete);        
               

?>




<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Recipe Description</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
  </head>

  <body>
      <!-- Navigation Bar -->    
     <nav class="navbar navbar-inverse navbar-fixed-top " id ="my-navbar">
      <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand"><strong> <i> Let's Cook  </i> </strong></a>
          </div>  <!-- navbar Header -->
          <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
              <li><a href="index.php">Home</a></li>
              <li><a href="topRecipe.php">Popular Recipes</a></li>
              <li><a href="recentlyAdded.php">Recently Added</a></li>
              <li><a href="help.php">Help</a></li>
              <li><a href="contact.php">Contact Us</a></li>
            </ul>
          </div>  <!-- End Collapse Navbar -->
            
      </div> <!-- End Container --> 
    </nav> <!-- End Navbar -->

    <div class="container-fluid" style="margin-top: 80px;  text-align: center; "> <!-- Start Div for Recipe's Name -->
    <h1 style="font-size: 30px;"> <?php if($row = mysqli_fetch_assoc($result)){ echo $row["recipe_name"]; } ?>  </h1>          
    </div> <!-- End Div for Recipe's Name -->

  <!-- Start Div for Ingredients -->
  <div style="margin-top: 50px; margin-left: 300px;margin-right: 300px;">
  <div class="row" >
  <div class="container-fluid" >
    <div class="col-lg-6">
      <h2 style="font-size: 20px; font-weight: bold;"> Ingredients  </h2>
        <?php
          if($serve_for == 2)
          {
              echo $row["ingredients_forTwo"];  
          }
          else if($serve_for == 4)
          {
              echo $row["ingredients_forFour"];
          } 
          else if($serve_for == 6)
          {
              echo $row["ingredients_forSix"];
          }
          else
          {
            echo $row["ingredient_description"];  
          }
           


        ?>
    </div>
    
    <div class="col-lg-6">
    <?php
       echo '<img src="data:image/jpeg;base64,'.base64_encode($row["picture"]).'" width="300px" height="200px"/>';
    ?>
    </div>
  </div>
  </div>
  </div>
    <!-- End Div for Ingredients -->




    <!-- Start Div for Directions  -->
  <div class="container-fluid" style="margin-left: 300px; margin-right: 300px;">
    <h2 style="font-size: 20px; font-weight: bold;"> Directions </h2>
         
      <p> <?php echo $row["description"]; ?> </p>
      

  </div>
  <!-- End Div for Directions -->

  


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>